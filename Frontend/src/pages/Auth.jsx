import { useState } from "react"
import RegisterForm from "../components/RegisterForm"
import LoginForm from "../components/loginForm"
import { register } from "../services/Api"
import { useAuth } from "../contexts/AuthContext"


function Auth() {
    const [isRegister, setIsRegister] = useState(false)
    
    const {state, login, register}  = useAuth()

    const handleSubmit = async (credentials) => {
        // if(isRegister) {
        //     register(credentials)
        // } else {
        //     const result = await login(credentials)
        //     console.log(result)
        //     dispatch({
        //         type: actionTypes.LOGIN_SUCCESS,
        //         data: result
        //     })
        // }

        if(isRegister) {
            register(credentials)
        } else {
            login(credentials)
        }

    }

    const handleRegisterClick = (event) => {
        event.preventDefault()
        setIsRegister(!isRegister)
    }

    return (
        <>
            <h1> Authentification </h1>
            {
                //Devant le ? on met la condition "si", derrière le ? on met le "alors", derrière  le : on met le "sinon"
                isRegister ? <RegisterForm onSubmit={handleSubmit}/> : <LoginForm onSubmit={handleSubmit}/>
            }

            <div>
                <a onClick={handleRegisterClick} href=""> 
                { 
                    isRegister ? "J'ai déjà un compte" : "Je n'ai pas de compte" 
                }
                </a>
            </div>
        </>

    )
}

export default Auth