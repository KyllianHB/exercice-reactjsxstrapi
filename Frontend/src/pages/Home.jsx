import { useAuth } from "../contexts/AuthContext"

function Home() {

    const {state, logout} = useAuth()

    return(
        <>
            <h1>HOMEUH</h1>
            <button onClick={logout}>Deconnexion</button>
        </>
    )
}

export default Home