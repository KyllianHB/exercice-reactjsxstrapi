import { useEffect, useState } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import RestaurantDetail from '../components/RestaurantDetail'
import { getRestaurantById } from '../services/Api'

function Restaurant () {
  // const { slug } = useParams()
  const { state: { id } } = useLocation()

  const [restaurant, setRestaurant] = useState()

  useEffect(() => {
    const getData = async () => {
      const result = await getRestaurantById(id)
      setRestaurant(result.data)
    }
    getData()
  }, [])

  if (!restaurant) {
    return <h1>Chargement ...</h1>
  }

  return (
    <>
      {/* <h1>{slug} - {id}</h1> */}
      <RestaurantDetail restaurant={restaurant} />
    </>
  )
}

export default Restaurant
