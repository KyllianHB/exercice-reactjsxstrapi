import { Link } from 'react-router-dom'
import { useAuth } from '../contexts/AuthContext'

function Navbar () {

  //on extraite la variable state et on extrait isAuthenticated et user de la variable state
  const { state: {isAuthenticated, user} } = useAuth()

  return (
    <nav>
      <ul>
        <li>
          <Link to='/'>Accueil</Link>
        </li>
        <li>
          <Link to='/about'>About</Link>
        </li>
        <li>
          <Link to='/restaurants'>Restaurants</Link>
        </li>
        <li>
          <Link to='/add-restaurant'>AJout Restaurants</Link>
        </li>
          {
            isAuthenticated ? (
              <li>Hello, {user.firstName}</li>
            )
            : (
              <li>
                <Link to='/auth'>S'inscrire / Se connecter</Link>
              </li>
            ) 
          }
      </ul>
    </nav>
  )
}

export default Navbar
