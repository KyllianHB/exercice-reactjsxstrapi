import axios from 'axios'
import { toast } from 'react-toastify'
import { saveToLocalStorage } from './LocalStorage'

const api = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
  header: {
    'Content-type': 'application/json',
    Accept: 'application/json'
  },
  timeout: 10000
})

const getRestaurants = async () => {
  try {
    const response = await api.get('/restaurants?populate=*')
    return response.data
  } catch (error) {
    console.error(error)
  }
}

const getRestaurantById = async (id) => {
  try {
    const response = await api.get(`/restaurants/${id}?populate=*`)
    return response.data
  } catch (error) {
    console.error(error)
  }
}

const getDishesByRestaurantId = async (id) => {
  try {
    const response = await api.get(`/dishes?filters[restaurant][id][$eq]=${id}&populate=*`)
    return response.data
  } catch (error) {
    console.error(error)
  }
}

const createRestaurant = async (formData) => {
  try {
    const _data = {
      data: {
        name: formData.name,
        description: formData.description,
        coordinates: {
          latitude: formData.latitude,
          longitude: formData.longitude
        }
      }
    }
    const response = await api.post('/restaurants', _data)
    return response.data
  } catch (error) {
    console.error(error)
  }
}

const login = async (credentials) => {

    
    const response = await api.post('/auth/local', credentials)
    return response.data

}

const register = async (credentials) => {
  // try {
    
  //   const response = await api.post('/auth/local/register', credentials)
  //   saveToLocalStorage('AUTHENTIFICATION', response)
  //   console.log(response.data)
  //   toast.success(`Félicitation M. ${response.data.user.firstName} ${response.data.user.lastName} pour votre inscription 🔥`)
  //   return response.data

  // } catch(error) {
  //   console.log(error)
  //   toast.error('Erreur de l\'inscription 🥺')
  // }
    
    const response = await api.post('/auth/local/register', credentials)
    return response.data

}

export {
  getRestaurants,
  getRestaurantById,
  createRestaurant,
  getDishesByRestaurantId,
  login,
  register
}
