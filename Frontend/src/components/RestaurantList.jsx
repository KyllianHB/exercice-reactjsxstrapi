import RestaurantListItem from './RestaurantListItem'
import '../style/RestaurantList.scss'

function RestaurantList ({ restaurants }) {
  return (
    <div className='list-container'>
      {
                restaurants.map(restaurant => {
                  return (
                    <RestaurantListItem key={restaurant.id} restaurant={restaurant} />
                  )
                })
            }
    </div>
  )
}

export default RestaurantList
