import { useState } from 'react'
import { createRestaurant } from '../services/Api'

function AddRestaurant () {
  // const [name, setName] = useState('')
  const [formData, setFormData] = useState({
    name: '',
    description: '',
    latitude: '',
    longitude: ''
  })

  const handleChangeName = (event) => {
    // setName(event.target.value)
    setFormData({
      ...formData, // recopie ce qu'il y'a deja dans l'objet formData
      [event.target.name]: event.target.value // remplace le champs qu'on a choisi de modifier
    })

    // console.log(formData)
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    if (formData.name && formData.description) {
      const result = await createRestaurant(formData)
      console.log(result)
    }
  }

  return (
    <>
      <h1>Ajouter un restaurant</h1>
      {/* <p>{name}</p> */}
      <form noValidate onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column' }}>
        <label>
          Nom :
          <input type='text' name='name' onChange={handleChangeName} value={formData.name} />
        </label>
        <label>
          Description :
          <textarea name='description' onChange={handleChangeName} value={formData.description} />
        </label>
        <label>
          Lattitude:
          <input type='text' name='latitude' onChange={handleChangeName} value={formData.latitude} />
        </label>
        <label>
          Longitude :
          <input type='text' name='longitude' onChange={handleChangeName} value={formData.longitude} />
        </label>
        <input type='submit' />
      </form>
    </>
  )
}

export default AddRestaurant
