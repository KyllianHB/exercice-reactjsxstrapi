import '../style/SubmitButton.scss'


function SubmitButton(props) {
    return (  
        <input className="submitButton" type="submit" {...props}/>
    );
}

export default SubmitButton;