import { useState } from "react"
import TextInput from "./TextInput"
import SubmitButton from "./submitButton"

function RegisterForm({onSubmit}) {

    
    const [credentials, setCredentials] = useState({
        email: 'foo@gmail.fr',
        username: '',
        password: 'foofoo',
        firstName: 'foo',
        lastName: 'Bar'
    })

    const handleChange = (event) => {
        const inputName = event.target.name 
        const inputValue = event.target.value
        
        setCredentials ({
            ...credentials,
            [inputName] : inputValue
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        if (credentials.email && credentials.password) {
            credentials.username = credentials.email
            onSubmit(credentials)
        }
    }

    return ( 
        <>
            <h2>Inscription</h2>
            
            <form noValidate onSubmit={handleSubmit}>
                <TextInput label="Nom:" name="lastName" type="text" placeholder="Bar"onChange={handleChange} value={credentials.lastName} />
                <br/>    
                <TextInput label="Prénom:" name="firstName" type="text" placeholder="Foo"onChange={handleChange} value={credentials.firstName} />
                <br/>    
                <TextInput label="Email:" name="email" type="email" placeholder="email@domain.fr" onChange={handleChange} value={credentials.email} />
                <br/>   
                <TextInput label="Mot de passe:" name="password" type="password" placeholder="*****"onChange={handleChange} value={credentials.password} />
                <br/>
                <SubmitButton value="S'inscrire"/>
            </form>
        </>
     );
}

export default RegisterForm;