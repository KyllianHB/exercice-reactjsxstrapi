import { useState } from "react"
function LoginForm({onSubmit}) {

    const [credentials, setCredentials] = useState({
        identifier: 'foo@bar.fr',
        password: 'password'
    })

    const handleChange = (event) => {
        const inputName = event.target.name 
        const inputValue = event.target.value
        
        setCredentials ({
            ...credentials,
            [inputName] : inputValue
        })
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        if (credentials.identifier && credentials.password) {
            onSubmit(credentials)
        }
    }

    return ( 
        <>
            <h2>Se connecter</h2>

            <form noValidate onSubmit={handleSubmit}>
                <label>
                    Email:
                    <input name="identifier" type="email" placeholder="email@domain.fr" onChange={handleChange} value={credentials.identifier} />
                </label>
                <br/>
                <label>
                    Mot de passe:
                    <input name="password" type="password" placeholder="*****"onChange={handleChange} value={credentials.password} />
                </label>
                <br/>
                <input type="submit" value="Se connecter"/>
            </form>
        </>
     );
}

export default LoginForm;