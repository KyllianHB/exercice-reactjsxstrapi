import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import DisheList from './DisheList'
import { getDishesByRestaurantId } from '../services/Api'


function RestaurantDetail ({ restaurant }) {
  
  const { state: { id } } = useLocation()

  const [dishes, setDishes] = useState()

  useEffect(() => {
    const getData = async () => {
      const result = await getDishesByRestaurantId(id)
      setDishes(result.data)
    }
    getData()
  }, [])

  console.log(dishes)

  return (
    <>
      <h1>{restaurant.attributes.name}</h1>
      <h1>plat</h1>
      <h1>-</h1>
      <DisheList dishes={dishes} />
    </>

  )
}
export default RestaurantDetail
