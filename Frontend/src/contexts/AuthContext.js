import React, { useEffect } from "react";
import { login, register } from "../services/Api";
import { toast } from "react-toastify";

const AuthContext = React.createContext();

const actionTypes = {
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_FAILURE: 'LOGIN_FAILURE',
    LOADING: 'LOADING',
    LOGOUT: 'LOGOUT',
    REGISTER_SUCCESS: 'REGISTER_SUCCESS',
    REGISTER_FAILURE: 'REGISTER_FAILURE',
}

const initialState = {
    isAuthenticated: false,
    token: null,
    user: null,
    loading: false,
    error: null
}


const AuthReducer = (state, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_SUCCESS:
            return{
                isAuthenticated: true,
                token: action.data.jwt,
                user: action.data.user,
                loading: false,
                error: null 
            }
        case actionTypes.LOGIN_FAILURE:
            return{
               ...initialState,
               error: action.data.error
            }
        case actionTypes.LOADING:
            return {
                ...state,
                loading: true
            }
        case actionTypes.LOGOUT:
            return {
                initialState
            }
        case actionTypes.REGISTER_SUCCESS:
            return{
                isAuthenticated: true,
                token: action.data.jwt,
                user: action.data.user,
                loading: false,
                error: null 
            }
        case actionTypes.REGISTER_FAILURE:
            return{
                ...initialState,
                error: action.data.error
            }
        default:
            throw new Error(`Unhandled action type : ${action.type}`)
    }
}

const AuthContextFactory = (dispatch) => ({
    login: async (credentials) => {

        console.log(credentials)

        try {
            //appel la fonction login dans api.js
            const result = await login(credentials)
            console.log(result)
            dispatch({
                type: actionTypes.LOGIN_SUCCESS,
                data: result
            })
            toast.success(`Bienvenue M. ${result.user.firstName} ${result.user.lastName} 🔥`)

            
        } catch (error) {
            console.log(error)
            dispatch({
                type: actionTypes.LOGIN_FAILURE,
                data: {error}
            })
            toast.error('Identifiant ou mot de passe invalid 🥺')
        }
    },
    logout: async () => {
        try {
            dispatch({
                type: actionTypes.LOGOUT,
            })
            toast.success('Aurevoir 👋')
        } catch(error) {
            toast.error('Utilisateur déjà déconnecté')
        }
    },
    register: async (credentials) => {
        try {
            const result = register(credentials)
            dispatch({
                type: actionTypes.REGISTER_SUCCESS,
                data: result
            })
            toast.success(`Félicitation M. ${result.user.firstName} ${result.user.lastName} pour votre inscription 🔥`)
        
          } catch(error) {

            console.log(error)
            dispatch({
                type: actionTypes.REGISTER_FAILURE,
                data: error
            })
            toast.error('Erreur de l\'inscription 🥺')
          }
    }
})

const AuthProvider = ({children}) => {

    const savedState = window.localStorage.getItem('AUTH')
    const _initialState = savedState ? JSON.parse(savedState) : initialState
    const [state, dispatch] = React.useReducer(AuthReducer, _initialState)

    useEffect(() => {
        window.localStorage.setItem('AUTH', JSON.stringify(state))
    }, [state])

    return (
        <AuthContext.Provider value={{state, ...AuthContextFactory(dispatch)}}>
            {children}
        </AuthContext.Provider>
    )
}

const useAuth = () => {
    const context = React.useContext(AuthContext)
    if(!context) throw new Error('Context doesnt exists')

    return context
}

export {
    AuthProvider,
    useAuth
}